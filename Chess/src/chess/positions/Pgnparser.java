package chess.positions;
 
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
 
public class Pgnparser {
	ArrayList<String> moves = new ArrayList<String>();
	Pgnparser() {
		
		try(BufferedReader br = new BufferedReader(new FileReader("src/pgnInput.txt"))){
				 
			String line ;
			while ((line = br.readLine()) != null) {
				
				StringTokenizer st = new StringTokenizer(line, " ");
				while (st.hasMoreTokens()) {
					
					moves.add((String) st.nextElement());
										
				}
			}	
		}
		catch (IOException e) {
			System.out.println("File not found");
		}
	}
	
	void nextmove() {
		Board b = new Board();
		String whitemove ;
		String blackmove ;
		int length = moves.size();
		for (int i = 0;i < length ;i++) {
			if(i%3 != 0) {
				 whitemove =  moves.get(i);
				 blackmove =  moves.get(i+1);
				 b.movePiece(whitemove,"W");
				 b.movePiece(blackmove,"B");
				 i++;	 
			}
		}
		b.display();		
	}
	public static void main(String[] args) {
		Pgnparser obj = new Pgnparser();
		obj.nextmove();
	}	
}
		
 
	
