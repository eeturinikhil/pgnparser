package chess.positions;

public class Board {

	final String  FILE = "abcdefgh";
	final String RANK = "12345678";
	static int MAX = 8;
	String[][] board = { { "WR", "WN", "WB", "WQ", "WK", "WB", "WN", "WR" },
			{ "WP", "WP", "WP", "WP", "WP", "WP", "WP", "WP" },
			{ "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
			{ "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
			{ "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
			{ "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
			{ "BP", "BP", "BP", "BP", "BP", "BP", "BP", "BP" },
			{ "BR", "BN", "BB", "BQ", "BK", "BB", "BN", "BR" } };

	public void movePiece(String nextPosition, String pieceColor) {
		if (nextPosition.equals("O-O-O") || nextPosition.equals("O-O")) {
			castling(nextPosition, pieceColor);
			return;
		}
		switch (nextPosition.charAt(0)) {
		case 'R':
			moveRook(nextPosition, pieceColor);
			return;
		case 'N':
			moveKnight(nextPosition, pieceColor);
			return;
		case 'B':
			moveBishop(nextPosition, pieceColor);
			return;
		case 'Q':
			moveQueen(nextPosition, pieceColor);
			return;
		case 'K':
			moveKing(nextPosition, pieceColor);
			return;
		default:
			movePawn(nextPosition, pieceColor);
			return;
		}
	}

	private boolean isOnBoard(int rank, int file) {
		if (rank >= 0 && rank < MAX && file >= 0 && file < MAX)
			return true;
		return false;

	}

	public void castling(String nextPosition, String pieceColor) {
		if (nextPosition.equals("O-O")) {
			kingSideCastling(pieceColor);
		} else {
			queenSideCastling(pieceColor);
		}
	}

	private void queenSideCastling(String pieceColor) {
		int[] ROW = { 0, 7 };
		int COL = 4;
		for (int row = 0; row < 2; row++) {
			if (board[ROW[row]][COL].equals(pieceColor + "K")) {
				board[ROW[row]][COL - 3] = pieceColor + "K";
				board[ROW[row]][COL] = "  ";
				board[ROW[row]][COL - 2] = pieceColor + "R";
				board[ROW[row]][COL - 4] = "  ";
			}
		}
	}

	private void kingSideCastling(String pieceColor) {
		int[] ROW = { 0, 7 };
		int COL = 4;
		for (int row = 0; row < 2; row++) {
			if (board[ROW[row]][COL].equals(pieceColor + "K")) {
				board[ROW[row]][COL + 2] = pieceColor + "K";
				board[ROW[row]][COL] = "  ";
				board[ROW[row]][COL + 1] = pieceColor + "R";
				board[ROW[row]][COL + 3] = "  ";
			}
		}
	}

	public void moveRook(String nextPosition, String pieceColor) {

		int[] pos = getPosition(nextPosition);
		int startIndex = pos[0];
		int currentFile = MAX;
		if (pos[1] != MAX)
			currentFile = FILE.indexOf(nextPosition.charAt(pos[1]));

		int file = FILE.indexOf(nextPosition.charAt(startIndex));
		int rank = RANK.indexOf(nextPosition.charAt(startIndex + 1));

		board[file][rank] = pieceColor + "R";
		clrCurrentRLocation(file, rank, currentFile, pieceColor);
	}

	private void clrCurrentRLocation(int file, int rank, int currentFile,
			String pieceColor) {
		int[] ROW = { +1, +2, +3, +4, +5, +6, +7, -1, -2, -3, -4, -5, -6, -7 };
		int[] COL = { +1, +2, +3, +4, +5, +6, +7, -1, -2, -3, -4, -5, -6, -7 };

		if (currentFile == MAX) {
			for (int i = 0; i < ROW.length; i++) {
				int row = rank + ROW[i];
				int col = file + COL[i];

				if (isOnBoard(row, col)) {
					if (board[row][file].equals(pieceColor + "B")) {
						board[row][file] = "  ";
						break;
					}
					if (board[rank][col].equals(pieceColor + "B")) {
						board[rank][col] = "  ";
						break;
					}

				}
			}
		} else {
			board[rank][currentFile] = "  ";
		}
	}

	private int[] getPosition(String nextPosition) {
		int startIndex;
		int currentFile = MAX;
		if (nextPosition.contains("x")) {
			if (isCurrentFileGiven(nextPosition, 3)) {
				startIndex = 3;
				currentFile = 1;
			}
			startIndex = 2;
		} else {
			if (isCurrentFileGiven(nextPosition, 2)) {
				startIndex = 2;
				currentFile = 1;

			}
			startIndex = 1;
		}
		int[] pos = { startIndex, currentFile };

		return pos;

	}

	private boolean isCurrentFileGiven(String nextPosition, int pos) {
		char file = nextPosition.charAt(pos);
		for (int i = 0; i < FILE.length(); i++) {
			if (file == FILE.charAt(i))
				return true;
		}
		return false;

	}

	public void moveKnight(String nextPosition, String pieceColor) {
		int[] pos = getPosition(nextPosition);
		int startIndex = pos[0];
		int currentFile = MAX;
		if (pos[1] != MAX)
			currentFile = FILE.indexOf(nextPosition.charAt(pos[1]));
		int file = FILE.indexOf(nextPosition.charAt(startIndex));
		int rank = RANK.indexOf(nextPosition.charAt(startIndex + 1));
		board[rank][file] = pieceColor + "N";
		clrCurrentNLocation(file, rank, pieceColor, currentFile);

	}

	private void clrCurrentNLocation(int file, int rank, String pieceColor, int currentFile) {
		if (currentFile == MAX) {
			int[] ROW = { 1, 1, -1, -1, 2, 2, -2, -2 };
			int[] COL = { 2, -2, 2, -2, 1, -1, 1, -1 };
		
			for (int index = 0; index < ROW.length; index++) {
				int row = rank + ROW[index];
				int col = file + COL[index];
				if (isOnBoard(row, col)) {
					if (board[row][col].equals(pieceColor + "N")) {
						board[row][col] = "  ";
						return;
					}
				}
			}
		}
			int[] ROW = {1,-1,+2,-2};
			for (int index = 0; index < ROW.length; index++) {
				int row = rank + ROW[index];
				if (isOnBoard(row, currentFile)) {
					if (board[row][currentFile].equals(pieceColor + "N"))
						board[row][currentFile] = "  ";
				}
		}
	}

	public void moveBishop(String nextPosition, String pieceType) {
		int startIndex;
		if (nextPosition.contains("x"))
			startIndex = 2;
		startIndex = 1;
		int file = FILE.indexOf(nextPosition.charAt(startIndex));
		int rank = RANK.indexOf(nextPosition.charAt(startIndex + 1));
		board[rank][file] = pieceType + "B";
		clrCurrentBLocation(file, rank, pieceType);
	}

	private void clrCurrentBLocation(int file, int rank, String pieceColor) {
		int[] ROW = { +1, +2, +3, +4, +5, +6, +7, -1, -2, -3, -4, -5, -6, -7 };
		int[] COL = { +1, +2, +3, +4, +5, +6, +7, -1, -2, -3, -4, -5, -6, -7 };

		for (int i = 1; i < ROW.length; i++) {
			int row = rank + ROW[i];
			int col = file + COL[i];

			if (isOnBoard(row, col)) {

				if (board[row][col].equals(pieceColor + "B")) {
					board[row][col] = "  ";
					break;
				}
			}
		}
	}

	public void moveQueen(String nextPosition, String pieceColor) {
		int startIndex;
		if (nextPosition.contains("x")) {
			startIndex = 2;
		} else {
			startIndex = 1;
		}
		int file = FILE.indexOf(nextPosition.charAt(startIndex));
		int rank = RANK.indexOf(nextPosition.charAt(startIndex));

		clrCurrentQLocation(pieceColor);
		board[rank][file] = pieceColor + "Q";

	}

	private void clrCurrentQLocation(String pieceColor) {
		for (int row = 0; row < MAX; row++) {
			for (int col = 0; col < MAX; col++) {
				if (board[row][col].equals(pieceColor + "Q")) {
					board[row][col] = "  ";
					return;
				}
			}
		}
	}

	public void moveKing(String nextPosition, String pieceColor) {
		int startIndex;
		if (nextPosition.contains("x")) {
			startIndex = 2;
		} else {
			startIndex = 1;
		}
		int file = FILE.indexOf(nextPosition.charAt(startIndex));
		int rank = RANK.indexOf(nextPosition.charAt(startIndex + 1));

		clrCurrentKLocation(file, rank, pieceColor);
		board[rank][file] = pieceColor + "K";
	}

	private void clrCurrentKLocation(int file, int rank, String pieceColor) {
		int[] ROW = { +1, +1, -1, -1, 0, 0, +1, -1 };
		int[] COL = { +1, -1, +1, -1, +1, -1, 0, 0 };

		for (int i = 1; i < ROW.length; i++) {
			int row = rank + ROW[i];
			int col = file + COL[i];

			if (isOnBoard(row, col)) {

				if (board[row][col].equals(pieceColor + "K")) {
					board[row][col] = "  ";
					break;
				}

			}
		}
	}

	public void movePawn(String nextPosition, String pieceColor) {
		String pawnPosition = "P" + nextPosition;
		int[] pos = getPosition(pawnPosition);
		int startIndex = pos[0];
		int currentFile = MAX;
		int file;
		int rank;
		if (pos[1] != MAX)
			currentFile = FILE.indexOf(nextPosition.charAt(pos[1]));
		if (nextPosition.contains("=")) {
			startIndex = 0;
			int newPieceIndex = nextPosition.indexOf('=');
			file = FILE.indexOf(nextPosition.charAt(startIndex));
			rank = RANK.indexOf(nextPosition.charAt(startIndex + 1));
			board[rank][file] = pieceColor
						+ nextPosition.charAt(newPieceIndex + 1);
		} else {
			startIndex = 0;
			file = FILE.indexOf(nextPosition.charAt(startIndex));
			rank = RANK.indexOf(nextPosition.charAt(startIndex + 1));
			board[rank][file] = pieceColor + "P";
		}
		clrCurrentPLocation(file, rank, pieceColor, pawnPosition,currentFile);	
	}

	private void clrCurrentPLocation(int file, int rank, String pieceColor,
			String pawnPosition, int currentFile) {
		int[] ROW = { +1, -1, -1, +1, 1, -1, 2, -2 };
		int[] COL = { -1, -1, 1, +1, 0, 0, 0, 0 };
		int startIndex;
		int endIndex;
		if (pawnPosition.contains("x")) {
			startIndex = 0;
			endIndex = 3;
		} else {
			startIndex = 4;
			endIndex = 8;
		}
		for (int i = startIndex; i < endIndex; i++) {
			int row = rank + ROW[i];
			int col = file + COL[i];
			if (isOnBoard(row, col)) {
				if (board[row][col].equals(pieceColor + "P")) {
					board[row][col] = "  ";
					break;
				}
			}
		}

	}

	public void display() {
		for (int rank = 0; rank < MAX; rank++) {
			for (int file = 0; file < MAX; file++) {
				System.out.print("\t" + board[rank][file]);
			}
			System.out.println("\n");
		}
	}
}
